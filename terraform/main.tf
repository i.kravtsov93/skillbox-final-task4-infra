terraform {
    required_providers {
      yandex = {
        source = "yandex-cloud/yandex"
      }
    }
  required_version = ">= 0.13"
}

provider "yandex" {
    token       = var.token
    cloud_id    = var.cloud_id
    folder_id   = var.folder_id
    zone        = var.zone
}

data "yandex_compute_image" "ubuntu-2204-1" {
    family = "ubuntu-2204-lts"
}

#Create gitlab
resource "yandex_compute_instance" "gitlab" {
    name        = "gitlab-vm"
    platform_id = "standard-v1"
    zone        = var.zone

    resources {
        cores  = 4
        memory = 8
    }

    boot_disk {
        initialize_params {
            image_id = "${data.yandex_compute_image.ubuntu-2204-1.id}"
            size = "64"
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.sn-1.id
        nat = true
    }

    metadata = {
        ssh-keys = "ubuntu:${file("/home/ikravtsov/.ssh/id_rsa.pub")}"
    }
}

#Create gitlab-runner
resource "yandex_compute_instance" "gitlab-runner" {
    name        = "gitlab-runner-vm"
    platform_id = "standard-v1"
    zone        = var.zone

    resources {
        cores  = 2
        memory = 4
    }

    boot_disk {
        initialize_params {
            image_id = "${data.yandex_compute_image.ubuntu-2204-1.id}"
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.sn-1.id
        nat = true
    }

    metadata = {
        ssh-keys = "ubuntu:${file("/home/ikravtsov/.ssh/id_rsa.pub")}"
    }
}

#Create vms for monitoring
resource "yandex_compute_instance" "vms-monitoring" {
    for_each    = toset(var.vm_monitoring)
    name        = each.value
    platform_id = "standard-v1"
    zone        = var.zone

    resources {
        cores  = 2
        memory = 4
    }

    boot_disk {
        initialize_params {
            image_id = "${data.yandex_compute_image.ubuntu-2204-1.id}"
            size = "10"
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.sn-1.id
        nat = true
    }

    metadata = {
        ssh-keys = "ubuntu:${file("/home/ikravtsov/.ssh/id_rsa.pub")}"
    }
}

#Create vm for app
resource "yandex_compute_instance" "vms-app"{
    for_each    = toset(var.vm_app)
    name        = each.value
    platform_id = "standard-v1"
    zone        = var.zone

    resources {
        cores   = 2
        memory  = 2
    }

    boot_disk {
        initialize_params {
            image_id = "${data.yandex_compute_image.ubuntu-2204-1.id}"          
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.sn-1.id
        nat = true
    }

    metadata = {
        ssh-keys = "ubuntu:${file("/home/ikravtsov/.ssh/id_rsa.pub")}"
    }
}

resource "yandex_vpc_network" "default" {
    name = "network-1"
}

resource "yandex_vpc_subnet" "sn-1" {
    zone            = "ru-central1-a"
    network_id      = yandex_vpc_network.default.id
    v4_cidr_blocks  = ["10.1.10.0/24"]
}

resource "yandex_lb_target_group" "my-lb-tg-1" {
    name        = "my-target-group-1"
    region_id   = "ru-central1"

    target {
        subnet_id = "${yandex_vpc_subnet.sn-1.id}"
        address   = "${yandex_compute_instance.vms-app["prod1"].network_interface.0.ip_address}"
    }

    target {
        subnet_id = "${yandex_vpc_subnet.sn-1.id}"
        address   = "${yandex_compute_instance.vms-app["prod2"].network_interface.0.ip_address}"
    }
}

resource "yandex_lb_network_load_balancer" "my-nw-lb-1" {
    name = "my-network-load-balancer-1"

    listener {
        name = "my-listener-1"
        port = 80
    }

    attached_target_group {
        target_group_id = "${yandex_lb_target_group.my-lb-tg-1.id}"

        healthcheck {
            name = "http"
            http_options {
                port = 80
            }
        }
    }
}

output "internal_ip_address_gitlab" {
  value = yandex_compute_instance.gitlab.network_interface.0.nat_ip_address 
}

output "internal_ip_address_gitlab-runner" {
  value = yandex_compute_instance.gitlab-runner.network_interface.0.nat_ip_address 
}

output "internal_ip_address_prometheus" {
  value = yandex_compute_instance.vms-monitoring["prometheus"].network_interface.0.nat_ip_address 
}

output "internal_ip_address_grafana" {
  value = yandex_compute_instance.vms-monitoring["grafana"].network_interface.0.nat_ip_address 
}

output "internal_ip_address_vm-test-1" {
  value = yandex_compute_instance.vms-app["test"].network_interface.0.nat_ip_address 
}

output "internal_ip_address_vm-prod-1" {
  value = yandex_compute_instance.vms-app["prod1"].network_interface.0.nat_ip_address 
}

output "internal_ip_address_vm-prod-2" {
  value = yandex_compute_instance.vms-app["prod2"].network_interface.0.nat_ip_address 
}

output internal_ip_address_laod_balancer {
  value = yandex_lb_network_load_balancer.my-nw-lb-1.listener
}