variable token {
  type        = string
  default     = ""
  description = "token for connection to YC"
}

variable cloud_id {
  type        = string
  default     = ""
  description = "cloud_id for connection to YC"
}

variable folder_id {
  type        = string
  default     = ""
  description = "folder_id for connection to YC"
}

variable zone {
  type        = string
  default     = ""
  description = "zone for connection to YC"
}

variable vm_app{
  type = set(string)
  default = ["test","prod1","prod2"]
  description = "VM for application"
}

variable vm_monitoring {
  type = set(string)
  default = ["grafana","prometheus"]
  description = "VMs for monitoring"
}